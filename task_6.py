from datetime import date


class MyClass1:
    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, surname, name, birthYear):
        return cls(surname, name, date.today().year - birthYear)

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))

    @classmethod
    def num_of_adults(cls, peoples):
        num = 0
        for people in peoples:
            if people.age >= 18:
                num += 1
        return num


peoples =[
    MyClass1('Ivanenko', 'Ivan', 19),
    MyClass1.fromBirthYear('Dovzhenko', 'Bogdan',  2000),
    MyClass1.fromBirthYear('Sydorchuk', 'Petro', 2010),
    MyClass1.fromBirthYear('Makuschenko', 'Dmytro', 2001)
]

print(MyClass1.num_of_adults(peoples))