from datetime import date


class MyClass1:
    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, surname, name, birthYear):
        return cls(surname, name, date.today().year - birthYear)

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))

    @staticmethod
    def legal_age(age):
        if age >= 18:
            return 'He/She is of legal age'
        
        else:
            return 'He/She is not of legal age'

class MyClass2(MyClass1):
    def get_legal_age(self):
        return MyClass1.legal_age(self.age)


m_per1 = MyClass2.fromBirthYear('Dovzhenko', 'Bogdan',  2000)
m_per1.print_info()
print(m_per1.get_legal_age())
print()

m_per2 = MyClass2.fromBirthYear('Sydorchuk', 'Petro', 2010)
m_per2.print_info()
print(m_per2.get_legal_age())
print()

m_per3 = MyClass2.fromBirthYear('Makuschenko', 'Dmytro', 2001)
m_per3.print_info()
print(m_per3.get_legal_age())
