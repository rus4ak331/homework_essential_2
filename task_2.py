class Rectangle:
    def __init__(self, width: int, height: int) -> None:
        self.width = width
        self.height = height

    def __str__(self) -> str:
        return f'Rectangle: \n\t width: {self.width}cm \n\t height: {self.height}cm'
    

class Mouse(Rectangle):
    def __init__(self, width: int, height: int) -> None:
        super().__init__(width, height)

    def right_mouse_click(self):
        self.width += 5 
        self.height += 5
        return 'You pressed the right mouse button'

    def left_mouse_click(self):
        self.width -= 5 
        self.height -= 5
        return 'You pressed the left mouse button'
    

class Button(Mouse):
    def __init__(self, width: int, height: int) -> None:
        super().__init__(width, height)

    def button(self):
        while True:
            print('Q - Stop')
            mouse_button = input('\nEnter mouse button(R or L): ')

            if mouse_button.upper() == 'R':
                self.right_mouse_click()
                print(f'\n {self}')

            elif mouse_button.upper() == 'L':
                if self.width and self.height <= 5:
                    print('\nRectangle height and width cannot be 0\n')
                    print(self)

                else:
                    self.left_mouse_click()
                    print(f'\n {self}')

            elif mouse_button.upper() == 'Q':
                break

            else:
                print('\nInvalid operation\n')


print('Click the right mouse button to increase the height and width of the rectangle by 5cm, left to decrease\n')

rectangle = Button(25, 15)

print(rectangle)
rectangle.button()