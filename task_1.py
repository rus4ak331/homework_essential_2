class Editor:
    def __init__(self, document: str) -> None:
        self.document = document
    
    def view_document(self):
        return f'Document: \n\t {self.document}'

    def edit_document(self):
        return 'Document editing is not available for the free version'


class ProEditor(Editor):
    def __init__(self, document: str) -> None:
        super().__init__(document)

    def edit_document(self):
        edit = input('edit the document: ')
        self.document = edit
        return '\nThe document has been edited'


document = 'Hello world'
license_key = 12345678

key = int(input('Enter license key: '))

if key == license_key:
    doc = ProEditor(document)
    print(doc.view_document(), '\n')
    print(doc.edit_document(), '\n')
    print(doc.view_document(),)

else:
    doc2 = Editor(document)
    print(doc2.view_document(), '\n')
    print(doc2.edit_document())
