class Animal:
    def __init__(self, name: str) -> None:
        self.name = name
        self.move = 'move'

    def breathe(self):
        return f'{self.name} breathes'
    

class Land(Animal):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self.legs = True
        self.move = 'walks'

    def walk(self):
        return f'{self.name} {self.move}'
    

class Aquatic(Animal):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self.fins = True
        self.move = 'swimming'

    def swim(self):
        return f'{self.name} {self.move}'
    

class Flying(Animal):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self.wings = True
        self.move = 'flying'

    def fly(self):
        return f'{self.name} {self.move}'
    

class Penguin(Land, Flying):
    def __init__(self, name: str) -> None:
        Land.__init__(self, name)
        super().__init__(name)
        

dog = Land('Dog')
print(dog.walk())
print()

fish = Aquatic('Fish')
print(fish.swim())
print()

bird = Flying('Bird')
print(bird.fly())
print()

penguin = Penguin('Penguin')
print(penguin.walk())
print(Penguin.__mro__)