class Transport:
    def __init__(self, name: str) -> None:
        self.name = name
        self.move = True


class Ship(Transport):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self.propeller_screw = True

    def swim(self):
        return f'The {self.name} can sail'  


class Car(Transport):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self.wheels = True

    def ride(self):
        return f'The {self.name} can drive'
    

class Plane(Transport):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self.wings = True

    def fly(self):
        return f'The {self.name} can fly'


class AircraftCarriers(Ship):
    def __init__(self, name: str) -> None:
        super().__init__(name)

    def airbase(self):
        return f'{self.name.title()} are a mobile air base on the water'
    

class PassengerCar(Car):
    def __init__(self, name: str) -> None:
        super().__init__(name)

    def shipping(self):
        return f'{self.name.title()} are needed to transport people'
    

class FlyingCar(Car, Plane):
    def __init__(self, name: str) -> None:
        super().__init__(name)


ship = Ship('ship')
print(ship.swim())
print()

car = Car('car')
print(car.ride())
print()

plane = Plane('plane')
print(plane.fly())
print()

aircraft_carriers = AircraftCarriers('aircraft carriers')
print(aircraft_carriers.swim())
print(aircraft_carriers.airbase())
print()

passenger_car = PassengerCar('passenger car')
print(passenger_car.ride())
print(passenger_car.shipping())
print()

flying_car = FlyingCar('flying car')
print(flying_car.fly())
print(flying_car.ride())
